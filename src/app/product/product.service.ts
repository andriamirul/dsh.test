import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Timestamp } from 'typeorm';
import { product_m } from './product.entity';
import { JwtService } from '@nestjs/jwt';
import { count } from 'console';

@Injectable()
export class ProductService {
  product: product_m[] = [];
  constructor(
    @InjectRepository(product_m)
    private productModel: Repository<product_m>,
    private jwtService: JwtService,
  ) {}

  async validateToken(token: string) {
    if (!token) {
      throw new HttpException(
        'Token harus disertakan',
        HttpStatus.UNAUTHORIZED,
      );
    }
    const validateToken = await this.jwtService.verify(token);
    const tokenValidThrough = new Date(validateToken.exp * 1000);
    const today = new Date();
    if (tokenValidThrough < today) {
      throw new HttpException(
        'Token sudah kadaluarsa',
        HttpStatus.UNAUTHORIZED,
      );
    }

    return validateToken;
  }
  async insertBarang(
    token: string,
    name: string,
    type: string,
    price: number,
    quantity: number,
  ) {
    try {
      const validateToken = await this.validateToken(token);
      if (!name || !type || !price || !quantity) {
        throw new HttpException(
          'Payload Tidak Sesuai',
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const role = validateToken.role;
      if (!role || role !== 1) {
        throw new HttpException(
          'Hanya admin yang dapat menambahkan barang',
          HttpStatus.UNAUTHORIZED,
        );
      }
      const date = new Date();
      const timestamp = date.getTime();
      const barangs = await this.getProducts(token);
      const jumBarang = 1 + barangs.length;
      const newUser = await this.productModel.insert({
        uuid:
          'BRG' +
          date.getFullYear() +
          date.getMonth() +
          date.getDay() +
          jumBarang,
        name: name,
        type: type,
        price: price,
        quantity: quantity,
        created_at: new Date().toString(),
      });
      return newUser;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async updateBarang(
    token: string,
    uuid: string,
    name: string,
    type: string,
    price: number,
    quantity: number,
  ) {
    try {
      const validateToken = await this.validateToken(token);
      if (!uuid) {
        throw new HttpException(
          'Payload Tidak Sesuai',
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const validateBarang = await this.getProduct(token, uuid);
      if (!validateBarang) {
        throw new HttpException('Barang tidak ditemukan', HttpStatus.NOT_FOUND);
      }
      const role = validateToken.role;
      if (!role || role !== 1) {
        throw new HttpException(
          'Hanya admin yang dapat update barang',
          HttpStatus.UNAUTHORIZED,
        );
      }
      const newUser = await this.productModel.update(
        { uuid: uuid },
        {
          name: name ? name : validateBarang.name,
          type: type ? type : validateBarang.type,
          price: price ? price : validateBarang.price,
          quantity: quantity ? quantity : validateBarang.quantity,
        },
      );
      console.log(newUser);
      return newUser;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async getProducts(
    token: string,
    limit: number = null,
    sortby: string = 'created_at',
    orderby: string = 'DESC',
  ) {
    const validateToken = await this.validateToken(token);
    const users = await this.productModel.findAndCount({
      order: {
        [sortby]: orderby,
      },
      take: limit,
    });
    return users;
  }

  async getProduct(token: string, uuid: string) {
    try {
      const validateToken = await this.validateToken(token);
      const product = await this.productModel.findOne({
        where: {
          uuid: uuid,
        },
      });
      return product;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async deleteProduct(token: string, uuid: string) {
    try {
      const validateToken = await this.validateToken(token);
      if (!uuid) {
        throw new HttpException(
          'Payload Tidak Sesuai',
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const validateBarang = await this.getProduct(token, uuid);
      if (!validateBarang) {
        throw new HttpException('Barang tidak ditemukan', HttpStatus.NOT_FOUND);
      }
      const role = validateToken.role;
      if (!role || role !== 1) {
        throw new HttpException(
          'Hanya admin yang dapat delete barang',
          HttpStatus.UNAUTHORIZED,
        );
      }
      const date = new Date();
      const formatedStringDate = date.getFullYear()+'-'+date.getMonth()+'-'+date.getDay()+' '+date.getTime();
      const newUser = await this.productModel.update(
        { uuid: uuid },
        { is_deleted: true, deleted_at: formatedStringDate },
      );
      console.log(newUser);
      return newUser;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
