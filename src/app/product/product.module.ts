import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { product_m } from './product.entity';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../constant';

@Module({
  imports: [
    TypeOrmModule.forFeature([product_m]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
  ],
  controllers: [ProductController],
  providers: [ProductService],
})
export class ProductModule {}
