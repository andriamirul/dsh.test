import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  Headers,
  Post,
} from '@nestjs/common';
import { ProductService } from './product.service';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post('add-product')
  async addProduct(
    @Headers('token') token: string,
    @Body('name') barangName: string,
    @Body('type') barangType: string,
    @Body('price') barangPrice: number,
    @Body('quantity') barangQty: number,
  ) {
    const user = await this.productService.insertBarang(
      token,
      barangName,
      barangType,
      barangPrice,
      barangQty,
    );
    return user;
  }

  @Post('update-product')
  async updateProduct(
    @Headers('token') token: string,
    @Body('uuid') barangUuid: string,
    @Body('name') barangName: string,
    @Body('type') barangType: string,
    @Body('price') barangPrice: number,
    @Body('quantity') barangQty: number,
  ) {
    const user = await this.productService.updateBarang(
      token,
      barangUuid,
      barangName,
      barangType,
      barangPrice,
      barangQty,
    );
    return user;
  }

  @Get('get-products')
  async getProducts(
    @Headers('token') token: string,
    @Body('limit') limit: number,
    @Body('sortby') sortby: string,
    @Body('orderby') orderby: string,
  ) {
    const Users = await this.productService.getProducts(
      token,
      limit,
      sortby,
      orderby,
    );
    return Users;
  }

  @Get('get-product')
  async getProduct(
    @Headers('token') token: string,
    @Body('uuid') uuid: string,
  ) {
    const user = await this.productService.getProduct(token, uuid);
    return user;
  }

  @Delete('delete-product')
  async deleteProduct(
    @Headers('token') token: string,
    @Body('uuid') uuid: string,
  ) {
    const user = await this.productService.deleteProduct(token, uuid);
    return user;
  }

  // @Post('login')
  // async login(
  //   @Body('email') UserEmail: string,
  //   @Body('password') UserPassword: string,
  // ){
  //   const response = await this.productService.login(
  //       UserEmail,
  //       UserPassword,
  //   );
  //   return response;
  // }
}
