import { Body, Controller, Get, Post } from '@nestjs/common';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('add-user')
  async addUser(
    @Body('email') UserEmail: string,
    @Body('password') UserPassword: string,
    @Body('role') UserRole: number,
  ) {
    const user = await this.userService.insertUser(
      UserEmail,
      UserPassword,
      UserRole,
    );
    return user;
  }

  @Get('get-users')
  async getUsers() {
    const Users = await this.userService.getUsers();
    return Users;
  }

  @Get('get-user')
  async getUser(@Body('username') userUsername: string) {
    const user = await this.userService.getUser(userUsername);
    return user;
  }

  @Post('login')
  async login(
    @Body('email') UserEmail: string,
    @Body('password') UserPassword: string,
  ) {
    const response = await this.userService.login(UserEmail, UserPassword);
    return response;
  }
}
