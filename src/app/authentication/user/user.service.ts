import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Model } from 'mongoose';
import { Repository } from 'typeorm';
import { user_m } from './user.entity';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UserService {
  user: user_m[] = [];
  constructor(
    @InjectRepository(user_m)
    private userModel: Repository<user_m>,
    private jwtService: JwtService,
  ) {}

  async insertUser(email: string, password: string, role: number) {
    try {
      if (!email || !password || !role) {
        throw new HttpException('Payload Tidak Sesuai', HttpStatus.NOT_ACCEPTABLE);
      }
      const newUser = await this.userModel.insert({
        email: email,
        password: password,
        role: role,
      });
      return newUser;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async getUsers() {
    const users = await this.userModel.find();
    return users;
  }

  async getUser(email: string) {
    try {
      const user = await this.userModel.findOne({
        where: {
          email: email,
        },
      });
      return user;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  async login(email: string, password: string) {
    try {
      if (!email || !password) {
        throw new HttpException(
          'Payload Tidak Sesuai',
          HttpStatus.NOT_ACCEPTABLE,
        );
      }
      const user = await this.getUser(email);
      if (!user) {
        throw new HttpException(
          'Email salah atau password salah',
          HttpStatus.UNAUTHORIZED,
        );
      }

      const userData = { email: email, role: user.role };
      const accessToken = this.jwtService.sign(userData);
      return {
        access_token: accessToken,
      };
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
