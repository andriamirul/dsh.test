import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class user_m {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  role: number;
}
